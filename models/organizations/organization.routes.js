const express = require('express');
const mongoose = require('mongoose');
const _ = require('lodash');
const moment = require('moment');

const organizations = require('./organization.controller');
const { verifyToken } = require('../../middleware/auth/jwt');


let ObjectId = mongoose.Types.ObjectId;
const router = express.Router();

router.get('/:id*?', verifyToken, (req, res, next) => {
    const query = { userId : req.userId}
    const response = {data: []}
    if(req.params['id']) query['deviceId'] = req.params['id']
    console.log(req.userId, query)
    organizations.find(query)
    .then(data => {
        response.message = 'Success',
        response.data = data
        res.status(200).send(response);
    })
    .catch(err => {
        response.message = 'Failed'
        res.status(500).send(response);
    });
});

router.post('/', verifyToken, (req, res, next) => {

    if(!ObjectId.isValid(req.body.deviceId)) {
        res.status(400).send('Bad Request');
        next();
        return;
    }

    const query = { userId : req.userId, deviceId: ObjectId(req.body.deviceId)}
    const project = {_id:0, _v:0}

    const response = {data: []}
    
    organizations.update(query, {...query, updatedAt: moment().utc().toDate() }, {upsert: true, setDefaultsOnInsert: true})
    .then(data => {
        response.message = 'Success',
        response.data = _.omit(query, ['_id', '__v', '$setOnInsert']),  
        res.status(200).send(response);
    })
    .catch(err => {
        console.log(err)
        response.message = 'Failed'
        res.status(500).send(response);
    });
});


module.exports = router;