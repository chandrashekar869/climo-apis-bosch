const mongoose = require('mongoose');
const moment = require('moment');

let Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

let organizationSchema = new Schema({
  userId: { type: ObjectId, required: true},
  deviceId: { type: ObjectId, require: true},
  createdAt: { type: Date, default: moment().utc().toDate()},
  updatedAt: { type: Date, default: moment().utc().toDate()}
});

try {
  module.exports = mongoose.model('organizations')
} catch (error) {
  module.exports = mongoose.model('organizations', organizationSchema);
}