const Organizations = require('./organization.model');

function insert(params) {
    let organization = new Organizations(params);
    return organization.save();
}

function find(params, project) {
    return Organizations.find(params, project).exec();
}

function update(query, data, options) {
    const organization = Organizations.findOneAndUpdate(query, data, options);
    return organization;
}

function remove(params) {
    return Organizations.deleteOne(params).exec();
}

function aggregations(pipeline) {
    return Organizations.aggregate(pipeline).exec();
}

module.exports = {
    insert,
    find,
    remove,
    update,
    aggregations
}