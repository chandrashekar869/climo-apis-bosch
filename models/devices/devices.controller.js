const Devices = require('./devices.model');

function insertMany(params) {
    const devices = Devices.insertMany(params);
    return devices;
}

function update(query, data, upsert) {
    const devices = Devices.updateOne(query, data, {upsert: upsert});
    return devices;
}

function get(params) {
    const devices = Devices.find(params);
    return devices;
}

function aggregations(pipeline) {
    const devices = Devices.aggregate(pipeline);
    return devices;
}

module.exports = {
    insertMany,
    update,
    get,
    aggregations
}