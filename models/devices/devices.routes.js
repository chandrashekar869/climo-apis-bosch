const express = require('express');
const _ = require('lodash');
const mongoose = require('mongoose');

const devices = require('./devices.controller');
const organizations = require('../organizations/organization.controller');
const { verifyToken } = require('../../middleware/auth/jwt');

const router = express.Router();
let ObjectId = mongoose.Types.ObjectId;

router.get('/', verifyToken, (req, res, next) => {
    const match = { userId : ObjectId(req.userId) };
    const lookup = {
        from: 'devices',
        localField: '_id',
        foreignField: '_id',
        as : 'thing'
    };
    const query  = [
        { $match : match }, 
        { $group : {"_id": "$deviceId"}}, 
        { $lookup : lookup},
        { $project : { _id : 0, __v:0 , "devices._id":0, "devices.__v": 0 }},
    ];
    const response = { result : [] };
    organizations.aggregations(query)
    .then((data) => {
        response.message = 'Success';
        response.result = [...data.map((d) => {
            const thing = d.thing[0];
            return {
                "thingId": thing._id,
                "thingKey": thing.key,
                "thingName": thing.name,
                "connected": thing.connected,
                "latitude": thing.loc.lat,
                "longitude": thing.loc.lng,
                "locUpdated": thing.locUpdated,
                "country": thing.loc.addr.country,
                "lastRecordTimeStamp": thing.lastCommunication,
                "macId": thing.attrs.mac_address.value,
                "city": thing.loc.addr.country,
                "lastUpdated": thing.updatedOn
              }
        })];
        response.count = data.length;
        res.status(200).send(response);
    })
    .catch((err) => {
        console.log(err);
        response.message = 'Failed';
        response.count = 0;
        res.status(500).send(response);
    })    
    // const response = { data : []};
    // devices.get().exec((err, result) => {
    //     if(err) {
    //         response.message = 'Error';
    //         res.status(500).send(response);
    //     } else {
    //         response.message = 'Success';
    //         response.result = result.map((res) => {
    //             return _.omit(res.toJSON(), ['_id', '__v'])
    //         });
    //         res.status(200).send(response);
    //     }
    // });
});


module.exports = router;