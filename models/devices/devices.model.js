const mongoose = require('mongoose');
const moment = require('moment');
let Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

let devicesSchema = new Schema({
    'alarms' : { type : Object },
    'apiCounts' : { type : Object },
    'attrs' : { type : Object },
    'connected' : { type : Boolean },
    'createdOn' : { type : Date },
    'defKey' : { type : String},
    'defName' : { type : String },
    'id' : { type : String},
    'key' : { type : String, unique : true},
    'lastCommunication' : { type : String},
    'lastSeen' : { type : Date},
    'loc' : { type : Object},
    'locUpdated' : { type : Date},
    'name' : { type : String},
    'ownerOrgId' : { type : String},
    'permission' : { type : String},
    'properties' : { type : Object},
    'proto' : { type : String},
    'remoteAddr' : { type : String},
    'updatedOn' : { type : Date},
    'varBillingPlanCode' : { type : String},
    'varVasPackageCode' : { type : String}
});

try {
    module.exports = mongoose.model('devices')
  } catch (error) {
    module.exports = mongoose.model('devices', devicesSchema);
  }