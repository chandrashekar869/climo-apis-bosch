const express = require('express');
const mongoose = require('mongoose');
const moment = require('moment');

const router = express.Router();

const calibrations = require('./calibration.controller');
const devices = require('../devices/devices.controller');

const { verifyToken } = require('../../middleware/auth/jwt');

const {historical, current, report} = require('../../joi/calibration/calibration');

let ObjectId = mongoose.Types.ObjectId;

router.post('/historical', verifyToken, (req, res, next) => {
    req.body.page = req.body.page >= 0? req.body.page : 0;
    req.body.limit = req.body.limit > 0? req.body.limit : 1;

    const {error, values} = historical.validate(req.body);
    const response = { result : [] };
    
    if(error) {
        res.status(400).send('Bad request');
        next();
        return;
    }

    const userExists = [
        {
            $match: {key : req.body.thingKey}
        },
        {
            $lookup : {
                from: 'organizations',
                localField: '_id',
                foreignField: 'deviceId',
                as : 'user'
            }
        },
        { $unwind: '$user'},
        {
            $match : {
                "user.userId" : ObjectId(req.userId)
            }
        }
    ]

    //Uncomment if manual authorization required
    // devices.aggregations(userExists).then((data) => {
    //     if(data.length) {
        devices.get({key: req.body['thingKey']}).then((data) => {

            if(data.length && req.organization.findIndex(organization => organization.deviceId == data[0]._id) != -1 ) {    
            calibrations.get({deviceId : ObjectId(data[0]._id), createdAt: { $gte : new Date(req.body.fromDate) , $lte: new Date(req.body.toDate) }}, {_id:0})
            .sort({createdAt: -1})
            .skip(req.body.page*req.body.limit)
            .limit(req.body.limit)
            .then((result) => {
                response.message = 'Success';
                response.result = result;
                response.page = req.body.page;
                response.limit = req.body.limit;
                res.status(200).send(response);
            })
            .catch(err => {
                console.error(err)
                response.message = 'Error';
                res.status(500).send('Error');
            })
        } else {
            response.message = 'Success';
            response.page = req.body.page;
            response.limit = req.body.limit;
            res.status(200).send(response);
        }
    }).catch(err => {
        console.error(err)
        res.status(500).send('Error');
    });
});



router.get('/current/:thingKey', verifyToken, (req, res, next) => {
    const {error, values} = current.validate(req.params);
    const response = { result : [] };
    if(error) {
        res.status(400).send('Bad request');
        next();
        return;
    }

    const userExists = [
        {
            $match: {key : req.params['thingKey']}
        },
        {
            $lookup : {
                from: 'organizations',
                localField: '_id',
                foreignField: 'deviceId',
                as : 'user'
            }
        },
        { $unwind: '$user'},
        {
            $match : {
                "user.userId" : ObjectId(req.userId)
            }
        }
    ]

    //Uncomment if manual authorization required
    
    // devices.aggregations(userExists).then((data) => {
    //     if(data.length) {
    devices.get({key: req.params['thingKey']}).then((data) => {
        if(data.length && req.organization.findIndex(organization => organization.deviceId == data[0]._id) != -1 ) {
            calibrations.getOne({thing_key: data[0].key}, {_id: 0, __v:0})
            .sort({"createdAt" : -1})
            .then((result) => {
                response.message = 'Success';
                response.result = result;
                res.status(200).send(response);
            })
            .catch(err => {
                response.message = 'Error';
                res.status(500).send('Error');
            })
        } else {
            response.message = 'Success';
            res.status(200).send(response);
        }
    }).catch(err => {
        res.status(500).send('Error');
    });
});

router.post('/report/:average', verifyToken ,(req, res, next) => {
    const {error, values} = report.validate({...req.params, ...req.body });
    if(error) {
        res.status(400).send('Bad request');
        next();
        return;
    }
    const response = {result:[]}
    const userExists = [
        {
            $match: {key : req.body['thingKey']}
        },
        {
            $lookup : {
                from: 'organizations',
                localField: '_id',
                foreignField: 'deviceId',
                as : 'user'
            }
        },
        { $unwind: '$user'},
        {
            $match : {
                "user.userId" : ObjectId(req.userId)
            }
        }
    ]    
    let pollutants = {};

    if(req.body.propertyTags && req.body.propertyTags.length) {
        req.body.propertyTags.map((property) => {
            pollutants[property] = {$avg : `$${property}`}
        });
    } else pollutants = {
        "SENS_PM10": { $avg : "$SENS_PM10" },
        "SENS_PM2P5": { $avg : "$SENS_PM2P5" },
        "SENS_OZONE": { $avg : "$SENS_OZONE" },
        "SENS_CARBON_MONOXIDE": { $avg : "$SENS_CARBON_MONOXIDE" },
        "SENS_SULPHUR_DIOXIDE": { $avg : "$SENS_SULPHUR_DIOXIDE" },
        "SENS_NITRIC_OXIDE": { $avg : "$SENS_NITRIC_OXIDE" },
        "SENS_NITROGEN_DIOXIDE": { $avg : "$SENS_NITROGEN_DIOXIDE"},
    }

    
    const grouping = {
        "_id": {
            "year": { "$year": "$createdAt" },
            "dayOfYear": { "$dayOfYear": "$createdAt" },
            "hour": { "$hour": "$createdAt" },
            "interval": {
                "$subtract": [ 
                  { "$minute": "$createdAt" },
                  { "$mod": [{ "$minute": "$createdAt"}, Number(req.params['average'])] }
                ]
            }
        },
        ...pollutants,
        "interval" : { $last: "$createdAt"},
        "thingKey" : { $last: "$thing_key"}
    } 

    //Uncomment if manual auth required and not jwt claims
    // devices.aggregations(userExists).then((data) => {
    //     if(data.length) {
    devices.get({key: req.body['thingKey']}).then((data) => {
        if(data.length && req.organization.findIndex(organization => organization.deviceId == data[0]._id) != -1 ) {
            calibrations.aggregations([
                {
                    $match: {
                        deviceId: data[0]._id,
                        createdAt : {
                            $gte : moment(req.body.fromDate).utc().toDate(),
                            $lt: moment(req.body.toDate).utc().toDate(),
                        }
                    }
                }, {
                    $group: grouping
                    
                }, {
                    $sort : {
                        interval : -1
                    }
                }, {
                    $project : {_id:0, __v:0}
                }
            ]).then(data => {
                response.message = "Success";
                response.result = data;
                res.status(200).send(response);
            }).catch(err => {
                response.message = "Failed";
                res.status(200).send(response);
            })
        } else {
            res.status(500).send('Error');
        }
    });

});

module.exports = router;