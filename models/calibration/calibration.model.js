const mongoose = require('mongoose');
const moment = require('moment');
let Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

let calibrationSchema = new Schema({
    "SENS_PM10" : {type: Number},
    "SENS_PM2P5" : {type: Number},
    "SENS_OZONE" :  {type: Number},
    "SENS_CARBON_MONOXIDE" : {type: Number},
    "SENS_NITROGEN_DIOXIDE" : {type: Number},
    "createdAt" : {type:Date, default: moment().utc().toDate()},
    "thing_key" : {type: String, required:true},
    "AQI" : {type:Number},
    "deviceId" : {type:ObjectId, required: true}
});

try {
    module.exports = mongoose.model('test_calibration1')
  } catch (error) {
    module.exports = mongoose.model('test_calibration1', calibrationSchema);
  }