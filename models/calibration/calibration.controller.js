const Calibrations = require('./calibration.model');

function get(query, project) {
    return Calibrations.find(query, project);
}

function getOne(query, project) {
    return Calibrations.findOne(query, project || {});
}

function aggregations(pipeline) {
    return Calibrations.aggregate(pipeline).exec();
}

module.exports = {
    get,
    getOne,
    aggregations
};