const express = require('express');
const moment = require('moment');
const passwordHash = require('password-hash');

const users = require('./users.controller');
const organizations = require('../organizations/organization.controller');

const {signup, login} = require('../../joi/users/users');
const { signToken, verifyToken } = require('../../middleware/auth/jwt');
const router = express.Router();

router.post('/', verifyToken ,(req, res, next) => {
    const {error, value} = signup.validate(req.body);
    const response = {};

    if(error) {
        res.status(400).send('Bad request');
        return;
    }

    users.insert(req.body)
    .then(data => {
        response.message = 'Success';
        response.data = data;
        res.status(200).send(response);
    })
    .catch(err => {
        if(err.code === 11000) {
            response.message = 'Specified email is already registered';
            res.status(500).send(response);
        }
        else {
            response.message = 'Error creating user';
            res.status(500).send(response);
        }
    });
});

router.post('/login', (req, res, next) => {
    const {error, value} = login.validate(req.body);
    const response = { data : [] }
    if(error) {
        res.status(400).send('Bad request');
        return;
    }

    users.find({email: req.body.username})
    .then(data => {
        if(passwordHash.verify(req.body.password, data.password)) {
            data.lastLogin = moment().utc().toDate();
            data.save((err, product) => {
                console.log(err)
            });
            response.message = 'Success';
            organizations.find({ userId: data._id }, { __v: 0, updatedAt:0, createdAt:0})
            .then( organization => {
                response.data = {
                    name: data.name,
                    token: signToken({ userId: data._id, data: organization })
                };
                res.status(200).send(response);
            })
            .catch(err => {
                response.data = {
                    name: data.name,
                    token: signToken({ userId: data._id })
                };
                res.status(200).send(response);
            })
        } else {
            response.message = 'Forbidden';
            res.sendStatus(403);
        }
    })
    .catch((err) => {
        response.message = 'Failed';
        res.status(500).send(response);
    });
})


module.exports = router;