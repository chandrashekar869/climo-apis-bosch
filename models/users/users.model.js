const mongoose = require('mongoose');
const passwordHash = require('password-hash');
let Schema = mongoose.Schema;
const moment = require('moment');

let usersSchema = new Schema({
  name: { type: String, required: true },
  username: { type: String },
  email: { type: String, required: true, unique: true },
  phone: { type: String },
  password: { type: String, required: true },
  address: { type: String },
  createdAt: { type: Date, default: moment().utc().toDate() },
  updatedAt: { type: Date, default: moment().utc().toDate() },
  lastLogin: { type: Date, default: moment().utc().toDate() },
  company: { type: String, default: null },
  jobTitle: { type: String }
});

usersSchema.pre('save', function (next) {
  var user = this;
  if (!passwordHash.isHashed(user.password)) {
    user.password = passwordHash.generate(user.password);
  }
  next();
});

try {
  module.exports = mongoose.model('users')
} catch (error) {
  module.exports = mongoose.model('users', usersSchema);
}