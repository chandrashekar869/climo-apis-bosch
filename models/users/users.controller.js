const Users = require('./users.model');

function insert(params) {
    const user = new Users(params);
    return user.save();
}

function find(params) {
    return Users.findOne(params).exec();
}

function update(query, update) {
    return Users.update(query, update);
}

module.exports = {
    insert,
    find,
    update
}