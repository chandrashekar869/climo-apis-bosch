const fs=require('fs');
const jwt=require('jsonwebtoken');
var path = require('path');

//Read public and private keys from files on server
const privateKey=fs.readFileSync(path.join(__dirname,"private.key"),'utf8');
const publicKey=fs.readFileSync(path.join(__dirname,"public.key"),'utf8');

var i  = 'Ambee';// Issuer 
var s  = 'admin@getambee.com';// Subject 
var a  = 'https://getambee.com'; // Audience

//Options to sign jwt
var signOptions={
    issuer:i,
    subject:s,
    audience:a,
    expiresIn: "12h",
    algorithm: "RS256"
};

//Options to verify jwt
var verifyOptions={
    issuer:i,
    subject:s,
    audience:a,
    expiresIn: "12h",
    algorithm: ["RS256"]
};

//returns token 
function signToken(payload){
    return jwt.sign(payload,privateKey,signOptions);
}

//returns token data if valid else return false for invalid token 
function verifyToken(req,res,next){
    try
    { 
        var authHeader=req.headers["authorization"];
        const token=authHeader.split(" ")[1];
        jwt.verify(token,publicKey,verifyOptions);
        req.userId=decode(token).payload.userId;
        req.organization=decode(token).payload.data;
        next();
    }
    catch(err){
        //Invalid token caught and unauthorized response sent
        res.status(401).send("Unauthorized");
    }
}

//returns token data if valid else return false for invalid token
function decode(token){
    var result=jwt.decode(token,{complete:true});
    return result!=null?result:false;
}

module.exports={signToken,verifyToken,decode};