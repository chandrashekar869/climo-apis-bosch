const mongoose = require('mongoose');
const compression = require('compression')
const express = require('express');
const bodyParser = require('body-parser');
const serverless = require('serverless-http');
const cors = require('cors');
const config = require('config');
const helmet = require('helmet');

const app = express();
const router = express.Router();
const devices = require('./models/devices/devices.routes');
const users = require('./models/users/users.routes');
const organizations = require('./models/organizations/organization.routes');
const calibrations = require('./models/calibration/calibration.routes');

mongoose.connect(config.mongo_connection_string);

app.use(compression());
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));

app.use('/devices', devices);
app.use('/user', users);
app.use('/organizations', organizations);
app.use('/readings', calibrations);

// app.get('/', (req, res, next) => {
//     res.send('started')
// })

// app.listen(3000, () => {
//     console.log('Started')
// });

module.exports.handler = serverless(app);