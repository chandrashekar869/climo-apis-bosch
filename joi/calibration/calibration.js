const Joi = require('@hapi/joi');

const historical = Joi.object({
    fromDate:Joi.string().required(),
    toDate: Joi.string().required(),
    page: Joi.number().integer().optional(),
    limit: Joi.number().integer().optional(),
    thingKey: Joi.string().required()
});

const current = Joi.object({
    thingKey: Joi.string().required()
});

const report = Joi.object({
    "fromDate": Joi.string().required(),
    "propertyTags": Joi.array().items(Joi.string()).optional(),
    "thingKey": Joi.string().required(),
    "toDate": Joi.string().required(),
    "average": Joi.number().required()
})


module.exports = {historical, current, report} 