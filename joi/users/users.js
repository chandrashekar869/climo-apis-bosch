const Joi = require('@hapi/joi');

const signup = Joi.object({
    name: Joi.string().required(),
    username: Joi.string(),
    email: Joi.string().email().required(),
    phone: Joi.string().optional(),
    password: Joi.string().required(),
    address: Joi.string().optional(),
    company:Joi.string().optional(),
    jobTitle: Joi.string().optional()
});

const login = Joi.object({
    username: Joi.string().email().required(),
    password: Joi.string().required()
});


module.exports = {signup, login}